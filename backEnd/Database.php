<?php

class Database{
  private static $conn = null;

  private function __construct(){
    self::$conn = new PDO("mysql:host=localhost;dbname=customers", "root", "Root#2018",array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    // self::$conn->exec("set names utf8");
    self::$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,0);
    self::$conn->setAttribute(PDO::ATTR_TIMEOUT, 600);
    self::$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    self::$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  }

  public static function getInstance(){
    if(self::$conn == null){
      new Database();
    }
    return self::$conn;
  }

}

 ?>
