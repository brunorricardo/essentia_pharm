<?php

require_once 'Database.php';

class Customer
{
    private $id;
    private $name;
    private $email;
    private $phone;
    private $photoPath;

    private $conn;

    public function __construct($id = null, $name = null, $email = null, $phone = null, $photoPath = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->photoPath = $photoPath;
        $this->conn = Database::getInstance();
    }

    private function getConnInstance()
    {
        $this->conn = Database::getInstance();
    }

    public function persistCustomer()
    {
        try {

            $this->conn->beginTransaction();

            $sql = "insert into customer (name, email, phone, photo_path, date_registry) values (:name, :email, :phone, :photo, NOW())";
            $pstm = $this->conn->prepare($sql);
            $pstm->bindValue(':name', $this->name);
            $pstm->bindValue(':email', $this->email);
            $pstm->bindValue(':phone', $this->phone);
            $pstm->bindValue(':photo', $this->photoPath);

            $pstm->execute();

            $this->conn->commit();

            return array('done' => true, 'msg' => 'Cliente cadastrado!');

        } catch (PDOException $error) {
            $this->conn->rollBack();
            return array('done' => false, 'msg' => $error->getMessage());
        }
    }

    public function editCustomer($id, $customer){
        try{
            $this->conn->beginTransaction();

            $sql = "update customer set name = :name, email = :email, phone = :phone, date_registry = NOW(), photo_path = :photo where customer_id = :id";

            $pstm = $this->conn->prepare($sql);
            $pstm->bindValue(':id', $id);
            $pstm->bindValue(':name', $customer['name']);
            $pstm->bindValue(':email', $customer['email']);
            $pstm->bindValue(':phone', $customer['phone']);
            $pstm->bindValue(':photo', $customer['photo']);

            $pstm->execute();
            $this->conn->commit();

            return array('done' => true, 'msg' => 'Cliente alterado!');

        }catch(PDOException $err){
            $this->conn->rollBack();
            return array('done' => false, 'msg' => $err->getMessage());
        }
    }

    public function getAll()
    {

        $conn = Database::getInstance();

        $todos = $conn->query("select c.customer_id, c.name, c.phone, c.email, c.photo_path as photo, date_format(c.date_registry, '%d/%m/%Y %H:%i') as data from customer c");
        return $todos->fetchAll();

    }

    public function findById($id)
    {
        $conn = Database::getInstance();

        $customer = $conn->query("select * from customer c where c.customer_id = {$id}");
        $customer = $customer->fetch();

        return new Customer($customer['customer_id'], $customer['name'], $customer['email'], $customer['phone'], $customer['photo_path'], $customer['date_registry']);
    }

    public function parseJson()
    {
        return array('name' => $this->name, 'email' => $this->email, 'phone' => $this->phone, 'customer_id' => $this->id, 'photo' => $this->photoPath);
    }

    public function deleteCustomer()
    {
        try {
            $this->conn->beginTransaction();
            $this->conn->exec("delete from customer where customer_id = {$this->id}");
            $this->conn->commit();
            return array('done' => true, 'msg' => 'Cliente excluido!');
        } catch (PDOException $err) {
            $this->conn->rollBack();
            return array('done' => false, 'msg' => $err->getMessage());
        }
    }
}