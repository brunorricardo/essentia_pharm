const app = angular.module('app', ['ngRoute', 'ui.utils.masks', 'file-model']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: 'pages/home.html',
            controller: 'HomeCtrl as home'
        })
        .when('/edit/:id', {
            templateUrl: 'pages/edit.html',
            controller: 'EditCtrl as edit'
        })
        .otherwise({
            redirectTo: '/home'
        });
});


app.factory('ClienteService', function ($http) {
    let service = {};

    service.add = function (cliente, foto) {
        let form_data = new FormData();
        form_data.append('action', 'add-cliente');
        form_data.append('file', foto);
        form_data.append('cliente', JSON.stringify(cliente));
        return $http.post('backEnd/cliente-route.php', form_data, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        });
    };

    service.edit = function (id, cliente, foto) {
        let form_data = new FormData();
        form_data.append('action', 'edit-cliente');
        form_data.append('file', foto);
        form_data.append('cliente', JSON.stringify(cliente));
        form_data.append('id', id);
        return $http.post('backEnd/cliente-route.php', form_data, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'Process-Data': false
            }
        });
    };

    service.getAll = function () {
        return $http.get('backEnd/cliente-route.php?action=get-all')
    };

    service.getOne = function (id) {
        return $http.get(`backEnd/cliente-route.php?action=get-one&id=${id}`)
    };

    service.deleteOne = function (id) {
        let data = {action: 'delete-one', id}
        return $http.post('backEnd/cliente-route.php', data, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
    };

    return service;

});

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            console.log(scope);
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.controller('HomeCtrl', function (ClienteService, $scope) {

        this.novo = {};
        this.clientes = [];

        this.salvarNovo = function () {
            ClienteService.add(this.novo, $scope.foto)
                .then(res => {
                    if (res.data.done) {
                        this.buscaTodos();
                    } else {
                        alert(res.data.msg);
                    }
                })
        }

        this.buscaTodos = function () {
            ClienteService.getAll().then(all => this.clientes = all.data);
        }

        this.excluirCliente = function (id) {
            console.log(id);
            ClienteService.deleteOne(id).then(res => {
                if (res.data.done) {
                    this.buscaTodos();
                } else {
                    alert(res.data.msg);
                }
            });
        }

    }
);

app.controller('EditCtrl', function ($routeParams, ClienteService, $scope, $location) {

    let self = this;
    this.idCustomer = $routeParams['id'];
    this.customer = {};

    this.getInitalData = function () {
        ClienteService.getOne(this.idCustomer).then(res => self.customer = res.data);
    }

    this.editarCliente = function () {
        ClienteService.edit(this.idCustomer, this.customer, $scope.foto)
            .then(res => {
                if (res.data.done) {
                    $location.path("/home");

                } else {
                    alert(res.data.msg);
                }
            });
    }

});

